# Process Spyer

Log every process (start / stop) of the config

## Config

File : ***configLinux.conf*** and ***configWindows.conf***

`
cadence=2
log=/home/max/Documents
notepad
xeyes
`

- cadence : checking every X seconds
- log : path to log
- list of process you want to check


## Run as service : Linux

You need to use systemd to create a service running in the background.

- Create a service file in /etc/systemd/system

`
touch /etc/systemd/system/process-logger.service
`
- Put the following text in this file : 

```
[Unit]
Description=Log wanted process
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
Restart=always
RestartSec=1
User=max
ExecStart=/home/max/Documents/AppService/process-logger

[Install]
WantedBy=multi-user.target
```

*Note : you need to change the **ExecStart** path with the correct one, leading to the executable*


- Reload all deamons

`
systemctl daemon-reload 
`

- Enable start on boot

`
systemctl enable process-logger
`

- Start service

`
systemctl start process-logger
`

- Check state of service

`
systemctl status process-logger
`

## Run as service : Windows

Comming soon...

`
sc.exe create ProcessLogger binPath= "cmd /c C:\Users\maxime.poulain\Documents\AppService\process-logger.exe"
`



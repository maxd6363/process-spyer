cmake_minimum_required(VERSION 3.16)
project(ProcessSpyerIdea C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_VERBOSE_MAKEFILE ON)

add_executable(ProcessSpyerIdea main.c src/bool.h src/config.c src/config.conf src/config.h src/configLoader.c src/configLoader.h src/error.c src/error.h src/list.c src/list.h src/log.c src/log.h src/run.bash src/state.h src/testeur.c src/testeur.h)
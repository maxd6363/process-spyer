/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: maxime.poulain
 *
 * Created on 6 juillet 2020, 14:17
 */

#include <stdlib.h>
#include <unistd.h>
#include "src/list.h"
#include "src/config.h"
#include "src/configLoader.h"
#include "src/testeur.h"
#include "src/log.h"
#include "src/bool.h"


extern Bool flag;

Config *globalConfig;

int startProcessLogging(int argc, char **argv) {

	#ifdef SERVICE_LINUX
	
		#ifdef VERBOSE
			freopen("/tmp/stdout.log","w",stdout);
			freopen("/tmp/stderr.log","w",stderr);
		#endif
		globalConfig = loadConfig("/configLinux.conf");

	#elif SERVICE_WIN		
	
		#ifdef VERBOSE
			freopen("C:\\stdout.log","w",stdout);
			freopen("C:\\stderr.log","w",stderr);
		#endif
		globalConfig = loadConfig("C:\\configWindows.conf");
	
	#endif

	if (globalConfig == NULL) {
		fprintf(stderr, "No config found, exit application !\n\n");
		#ifdef VERBOSE
			fclose(stdout);
			fclose(stderr);
		#endif
		return -1;
	}
	displayConfig(globalConfig);
	logOut("Starting the app !", INTERNAL);


	#ifdef VERBOSE
		fclose(stdout);
		fclose(stderr);
	#endif

	run();
	return 0;
}







#ifdef SERVICE_WIN

#include <windows.h>
#include <windows.h>

SERVICE_STATUS ServiceStatus; 
SERVICE_STATUS_HANDLE hStatus; 

void  ServiceMain(int argc, char** argv); 
void  ControlHandler(DWORD request); 


int main(int argc, char** argv) { 
	SERVICE_TABLE_ENTRY ServiceTable[2];
	ServiceTable[0].lpServiceName = "ProcessLogger";
	ServiceTable[0].lpServiceProc = (LPSERVICE_MAIN_FUNCTION)ServiceMain;
	ServiceTable[1].lpServiceName = NULL;
	ServiceTable[1].lpServiceProc = NULL;
	StartServiceCtrlDispatcher(ServiceTable);  

	
}


void ServiceMain(int argc, char** argv) {
	ServiceStatus.dwServiceType        = SERVICE_WIN32; 
	ServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
	ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_SHUTDOWN;
	ServiceStatus.dwWin32ExitCode      = 0; 
	ServiceStatus.dwServiceSpecificExitCode = 0; 
	ServiceStatus.dwCheckPoint         = 0; 
	ServiceStatus.dwWaitHint           = 0; 

	hStatus = RegisterServiceCtrlHandler("ProcessLogger", (LPHANDLER_FUNCTION)ControlHandler); 
	if (hStatus == (SERVICE_STATUS_HANDLE)0) { 
		return; 
	}  
	ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
	SetServiceStatus (hStatus, &ServiceStatus);
	startProcessLogging(argc,argv);

}


void ControlHandler(DWORD request) { 
	switch(request) { 
		case SERVICE_CONTROL_STOP:
		case SERVICE_CONTROL_SHUTDOWN: 
		flag = false;
		ServiceStatus.dwWin32ExitCode = 0; 
		ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
		SetServiceStatus (hStatus, &ServiceStatus);
		return;
	} 
	SetServiceStatus(hStatus, &ServiceStatus);
} 





#elif SERVICE_LINUX

int main(int argc, char const *argv[]) {
	return startProcessLogging(argc, argv);
}


#endif










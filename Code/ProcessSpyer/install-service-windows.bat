@echo off
@setlocal enableextensions
@cd /d "%~dp0"

net session
if %errorlevel% == 2 (
	echo Please run as root/administrator
	GOTO:EOF
	)
	

echo CAUTION : You need to change to "log" property in configWindows.conf
pause

xcopy configWindows.conf C:\ /Y

sc.exe stop ProcessLogger
sc.exe delete ProcessLogger
sc.exe create ProcessLogger binPath= "cmd /c %CD%\debugWin\process-logger.exe"
sc.exe start ProcessLogger
sc.exe query ProcessLogger

pause
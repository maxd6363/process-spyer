/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "configLoader.h"
#include "error.h"
#include "list.h"

#define STRING_SIZE 256

Config *loadConfig(char *filename) {
    Config *config;
    if (filename == NULL) {
        error("Filename is null", __FILE__, __LINE__);
        return NULL;
    }
    FILE *configFile = fopen(filename, "r");
    if (configFile == NULL) {
        error("Unable to open file", __FILE__, __LINE__);
        fprintf(stderr, "File not found : %s\n", filename);
        return NULL;
    }
    char line[STRING_SIZE];
    int intTrash;
    char charTrash[STRING_SIZE];
    char *logLocation = (char*)malloc(STRING_SIZE * sizeof(char));
    if(logLocation == NULL){
        return NULL;
    }
    int cadence = 10;
    int indexLine = 0;
    List list = list_new();

    while (fgets(line, sizeof(line), configFile)!=NULL) {
        if (sscanf(line, "cadence=%d",&intTrash) == 1) {
            sscanf(line, "cadence=%d", &cadence);
        } else if(sscanf(line, "log=%s",charTrash) == 1){
            sscanf(line, "log=%s", logLocation);
        } else {
            if(strlen(line)<=1){
                continue;
            }
            line[strlen(line) - 1] = '\0';
            char *tmp = (char *) malloc(STRING_SIZE * sizeof(char));
            if (tmp == NULL) {
                error("malloc", __FILE__, __LINE__);
                return NULL;
            }
            strcpy(tmp, line);
            list_add(&list, tmp);
            indexLine++;
        }
    }


    char **process = (char **) malloc(indexLine * sizeof(char *));
    if (process == NULL) {
        error("Malloc", __FILE__, __LINE__);
        return NULL;
    }
    for (int i = 0; i < indexLine; i++) {
        process[i] = (char *) malloc(STRING_SIZE * sizeof(char));
        if (process[i] == NULL) {
            error("Malloc", __FILE__, __LINE__);
            return NULL;
        }
        strcpy(process[i], list_get(list, i));
    }


    list_free(&list);


    config = (Config *) malloc(sizeof(Config));
    if (config == NULL) {
        error("Malloc", __FILE__, __LINE__);
        return NULL;
    }

    config->size = indexLine;
    config->cadence = cadence;
    config->arrayProcess = process;
    config->logLocation = logLocation;


    fclose(configFile);

    return config;
}

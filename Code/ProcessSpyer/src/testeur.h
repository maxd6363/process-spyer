/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   testeur.h
 * Author: maxime.poulain
 *
 * Created on 7 juillet 2020, 09:12
 */

#ifndef TESTEUR_H
#define TESTEUR_H

#include "config.h"
#include "list.h"


void checkProcess(void);
List parseFile(void);
void run(void);

#endif /* TESTEUR_H */


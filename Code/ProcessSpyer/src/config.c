//
// Created by maxime.poulain on 07/07/2020.
//



#include "config.h"
#include "error.h"
#include <stdio.h>

void displayConfig(Config *config) {
    if (config == NULL) {
        error("Config null", __FILE__, __LINE__);
        return;
    }

    printf("Config : \n");
    printf("\tCadence : %d s\n", config->cadence);
    printf("\tLog location : %s\n", config->logLocation);
    printf("\tWatching : \n");
    for (int i = 0; i < config->size; i++) {
        printf("\t\t- %s\n", config->arrayProcess[i]);
    }
    printf("\n");
}


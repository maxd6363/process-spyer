/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "testeur.h"
#include "error.h"
#include "list.h"
#include "log.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define STRING_SIZE 128

extern Config *globalConfig;


const char *LINUX_COMMAND = "ps -aux";
const char *WINDOWS_COMMAND = "tasklist";
char *TMP_FILE;


List lastProcess;
Bool flag = true;
char command[256];


void processTmpAndCommand(void) {
#ifdef SERVICE_LINUX
    TMP_FILE = "/tmp/tmpProcess";
    sprintf(command, "%s > %s", LINUX_COMMAND, TMP_FILE);
#elif SERVICE_WIN
    TMP_FILE = "C:\\tmpProcess";
    sprintf(command, "%s > %s", WINDOWS_COMMAND, TMP_FILE);
#else
    fprintf(stderr,
        "NO PLATEFORME SELECTED DURING COMPILATION\nPLEASE USE -DSERVICE_WIN OR -DSERVICE_LINUX TO RESOLVE THE PROBLEME\n");
    exit(-1);
#endif
}


void checkProcess(void) {
    system(command);
    parseFile();
}


List parseFile(void) {
    char bufferLine[256];
    int length = 0;
    List list;
    list = list_new();


    FILE *cmd = fopen(TMP_FILE, "r");
    if (cmd == NULL) {
        error("fopen", __FILE__, __LINE__);
        printf("FILE NOT FOUND : %s\n", TMP_FILE);
        return NULL;
    }

    while (fgets(bufferLine, sizeof(bufferLine), cmd) != NULL) {
        bufferLine[strlen(bufferLine) - 1] = '\0';
        for (int i = 0; i < globalConfig->size; i++) {
            if (strstr(bufferLine, globalConfig->arrayProcess[i]) != NULL) {
                list_add(&list, globalConfig->arrayProcess[i]);
                length++;
            }
        }
    }
    fclose(cmd);


    return list;
}

void run(void) {
    processTmpAndCommand();
    while (flag) {       

        #ifdef SERVICE_LINUX
            #ifdef VERBOSE
                freopen("/tmp/stdout.log","a",stdout);
                freopen("/tmp/stderr.log","a",stderr);
            #endif
        #elif SERVICE_WIN       
            #ifdef VERBOSE
                freopen("C:\\stdout.log","a",stdout);
                freopen("C:\\stderr.log","a",stderr);
            #endif
        #endif
        checkProcess();
        List currentProcess = parseFile();
        if (lastProcess == NULL) {
            lastProcess = currentProcess;
            logProcess(lastProcess, NULL);
        } else {
            int sizeLast = list_size(lastProcess);
            int sizeCurrent = list_size(currentProcess);
            List started = list_new();
            List ended = list_new();

            for (int i = 0; i < sizeCurrent; ++i) {
                char *prog = list_get(currentProcess, i);
                if (prog != NULL) {
                    if (!list_contain(lastProcess, prog)) {
                        list_add(&started, prog);
                    }
                }
            }

            for (int i = 0; i < sizeLast; ++i) {
                char *prog = list_get(lastProcess, i);
                if (prog != NULL) {
                    if (!list_contain(currentProcess, prog)) {
                        list_add(&ended, prog);
                    }
                }
            }
            logProcess(started, ended);
            list_free(&lastProcess);
            lastProcess = currentProcess;
            list_free(&started);
            list_free(&ended);
        }
        #ifdef VERBOSE
            fclose(stdout);
            fclose(stderr);
        #endif
        sleep(globalConfig->cadence);
    }
}
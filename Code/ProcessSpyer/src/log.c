/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


#include "log.h"
#include "state.h"
#include "error.h"
#include "bool.h"
#include "config.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define STRING_SIZE 256


extern Config* globalConfig;

char *fileName;
FILE *file;

void logOut(char *prog, State state) {
    if (prog == NULL) {
        return;
    }
    openFile();

    if (file != NULL) {

        char *strState;
        switch (state) {
            case START:
                strState = " -> START";
                break;
            case STOP:
                strState = " -> STOP";
                break;
            case INTERNAL:
                strState = "";
                break;
            default:
                strState = " -> UNKNOWN";
                break;
        }
        fprintf(file, "[%s] : %s %s\n", getCurrentDate(), prog, strState);
        fprintf(stdout, "[%s] : %s %s\n", getCurrentDate(), prog, strState);
    }

    closeFile();
}

void generateFileName(void) {
    fileName = getFileName();
}

char *getFileName(void) {
    char *buffer = (char *) malloc(100 * sizeof(char));
    if (buffer == NULL) {
        error("malloc", __FILE__, __LINE__);
        return NULL;
    }
    time_t now;
    now = time(&now);
    struct tm *local = localtime(&now);
    sprintf(buffer, "log_%d-%02d-%02d.log", local->tm_year + 1900, local->tm_mon + 1, local->tm_mday);
    return buffer;
}

char *getCurrentDate(void) {
    char *buffer = (char *) malloc(100 * sizeof(char));
    if (buffer == NULL) {
        error("malloc", __FILE__, __LINE__);
        return NULL;
    }
    time_t now;
    now = time(&now);
    struct tm *local = localtime(&now);
    sprintf(buffer, "%d-%02d-%02d %02d:%02d:%02d", local->tm_year + 1900, local->tm_mon + 1, local->tm_mday,
            local->tm_hour, local->tm_min, local->tm_sec);
    return buffer;
}

void openFile() {
    if(globalConfig == NULL){
        fprintf(stderr, "No config found !\n");
        return;
    }
    generateFileName();
    char absoluteLogPath[STRING_SIZE];
    #ifdef SERVICE_LINUX
    sprintf(absoluteLogPath,"%s/%s",globalConfig->logLocation, fileName);
    #elif SERVICE_WIN
    sprintf(absoluteLogPath,"%s\\%s",globalConfig->logLocation, fileName);
    #endif
    file = fopen(absoluteLogPath, "a");
    if(file == NULL){
        error("fopen",__FILE__,__LINE__);
        fprintf(stderr, "File not found or no right: %s\n", absoluteLogPath);
    }
}

void closeFile() {
    if (file != NULL) {
        fclose(file);
    }
}

Bool doesFileExist() {
    char absoluteLogPath[STRING_SIZE];
    sprintf(absoluteLogPath,"%s/%s",globalConfig->logLocation, fileName);
    if ((file = fopen(absoluteLogPath, "r"))) {
        fclose(file);
        return true;
    }
    return false;
}

void logOutStart(char *prog) {
    logOut(prog, START);
}

void logOutStop(char *prog) {
    logOut(prog, STOP);
}


void logProcess(List started, List ended) {
    if (started != NULL)
        list_foreach(started, logOutStart);
    if (ended != NULL)
        list_foreach(ended, logOutStop);
}

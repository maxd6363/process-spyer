/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include <stdio.h>
#include "error.h"

void error(char *error, char *file, int line) {
	fprintf(stderr, "Error : %s, %s:%d\n", error, file, line);
}

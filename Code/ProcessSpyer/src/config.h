/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   config.h
 * Author: maxime.poulain
 *
 * Created on 6 juillet 2020, 14:19
 */

#ifndef CONFIG_H
#define CONFIG_H


typedef struct {
    int size;
    int cadence;
    char *logLocation;
    char **arrayProcess;
} Config;

void displayConfig(Config *config);



#endif /* CONFIG_H */


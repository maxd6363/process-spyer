#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define RED         "\x1b[31m"
#define GRE         "\x1b[32m"
#define YEL         "\x1b[33m"
#define BLU         "\x1b[34m"
#define MAG         "\x1b[35m"
#define CYA         "\x1b[36m"
#define RESET       "\x1b[0m"


#ifndef LIST_H
#define LIST_H

#include "bool.h"


typedef struct node{
	char * data;
	struct node *next;
}Node, *List;



List list_new(void);
void list_add(List *list, char* data);
void list_free(List *list);
void list_print(List list);
char* list_get(List list, int index);
int list_size(List list);
void list_foreach(List list, void(*foo)(char * data));
Bool list_contain(List list, char *data);

#endif //LIST_H
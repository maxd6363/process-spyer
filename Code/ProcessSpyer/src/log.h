/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   log.h
 * Author: maxime.poulain
 *
 * Created on 6 juillet 2020, 16:00
 */

#ifndef LOG_H
#define LOG_H

#include "state.h"
#include "bool.h"
#include "list.h"


void logOut(char *prog, State state);

char *getCurrentDate(void);

char *getFileName(void);

void generateFileName(void);

void openFile();

void closeFile();

Bool doesFileExist();

void logProcess(List started, List ended);


#endif /* LOG_H */


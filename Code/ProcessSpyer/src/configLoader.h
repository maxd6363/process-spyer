/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   configLoader.h
 * Author: maxime.poulain
 *
 * Created on 6 juillet 2020, 14:18
 */

#ifndef CONFIGLOADER_H
#define CONFIGLOADER_H

#include "config.h"


Config *loadConfig(char *filename);



#endif /* CONFIGLOADER_H */


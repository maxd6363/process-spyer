#include "list.h"


#include <string.h>



List list_new(void){
	return NULL;
}


void list_add(List *list, char* data){
	Node *new = (Node*)malloc(sizeof(Node));
	if(new == NULL){
		return;
	}
	new->data=data;
	new->next=*list;
	*list=new;
}


void list_free(List *list){
	if(list == NULL || *list == NULL) return;
	Node *last = *list;
	Node *tmp = *list;
	while(tmp != NULL){
		tmp=tmp->next;
		if(last != NULL)
			free(last);
		last=tmp;
	}
	*list=NULL;

}


void list_print(List list){
	Node *tmp = list;
	while(tmp != NULL){
		printf("%s\n",tmp->data);
		tmp=tmp->next;
	}
}


char* list_get(List list, int index){
	Node *tmp = list;
	int size=0;
	while(tmp != NULL){
		if(size == index){
			return tmp->data;
		}
		size++;
		tmp=tmp->next;
	}
	return NULL;
}


int list_size(List list){
	Node *tmp = list;
	int size=0;
	while(tmp != NULL){
		size++;
		tmp=tmp->next;
	}
	return size;
}

void list_foreach(List list, void(*foo)(char * data)){
	Node *tmp = list;
	while(tmp != NULL){
		foo(tmp->data);
		tmp=tmp->next;
	}
}


Bool list_contain(List list, char *data){
	Node *tmp = list;
	while(tmp != NULL){
		if(data != NULL){
			if(strcmp(data,tmp->data) == 0){
				return true;
			}

		}
		tmp=tmp->next;
	}
	return false;
}
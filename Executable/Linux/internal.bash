#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root/administrator"
  exit
fi

cp "configLinux.conf" "/configLinux.conf"
chmod 755 "/configLinux.conf"



serviceFile="/etc/systemd/system/process-logger.service"

sudo touch $serviceFile

echo "[Unit]
Description=Log wanted process

[Service]
Type=simple
Restart=always
RestartSec=1
ExecStart=$(pwd)/process-logger

[Install]
WantedBy=multi-user.target
" > $serviceFile

systemctl stop process-logger
systemctl disable process-logger
systemctl daemon-reload 
systemctl reset-failed

systemctl enable process-logger
systemctl start process-logger
systemctl status process-logger






#!/bin/bash


if [ "$EUID" -ne 0 ]
  then echo "Please run as root/administrator"
  exit
fi

systemctl stop process-logger
systemctl disable process-logger
systemctl daemon-reload 
@echo off


net session
if %errorlevel% == 2 (
	echo Please run as root/administrator
	pause
	GOTO:EOF
	)



sc.exe stop ProcessLogger
sc.exe delete ProcessLogger
pause
